import { Roles } from './../core/security';

export type User = {
  id: string
  email: string
  hashedPassword: string
  salt?: string
  role: Roles
};

export type UserStorage = {
  id: string
  email: string
  hashed_password: string
  salt?: string
  role: Roles
};

export type SignIn = {
  token: string
};
