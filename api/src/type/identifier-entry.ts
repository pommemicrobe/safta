export type IdentifierEntry = {
  id: string
  name: string
  url: string
  encryptedUsername: string
  encryptedPassword: string
  userId: string
};

export type IdentifierEntryStorage = {
  id: string
  name: string
  url: string
  encrypted_username: string
  encrypted_password: string
  user_id: string
};
