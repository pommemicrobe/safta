import { IdentifierEntry } from './identifier-entry';

export type Entries = {
  identifierEntries: IdentifierEntry[] | null
}
