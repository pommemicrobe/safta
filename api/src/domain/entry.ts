import { Entries } from './../type/entry';
import { findEntries as findEntriesController } from './../controller/entry';
import { register } from './../core/register';

register({
  schema: {
    types: `
      type Entries {
        identifierEntries: [IdentifierEntry]
      }
    `,
    queries: `
      findEntries(userId: ID!): Entries
    `,
  },
  controller: {
    queries: {
      findEntries: (_parent: any, args: any, context: any): Promise<Entries | null> => {
        return findEntriesController(args, context);
      },
    },
  }
});
