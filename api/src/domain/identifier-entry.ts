import { IdentifierEntry } from './../type/identifier-entry';
import { register } from './../core/register';
import {
  createIdentifierEntry as createIdentifierEntryController,
  deleteIdentifierEntry as deleteIdentifierEntryController,
  updateIdentifierEntry as updateIdentifierEntryController,
} from './../controller/identifier-entry';

register({
  schema: {
    types: `    
      input CreateIdentifierEntryInput {
        userId: ID!
        name: String!
        url: String
        encryptedUsername: String!
        encryptedPassword: String!
      }
    
      input UpdateIdentifierEntryInput {
        userId: ID!
        name: String
        url: String
        encryptedUsername: String
        encryptedPassword: String
      }
    
      type IdentifierEntry {
        id: ID
        name: String
        url: String
        encryptedUsername: String
        encryptedPassword: String
        userId: ID
      }
    `,
    mutations: `
      createIdentifierEntry(input: CreateIdentifierEntryInput!): IdentifierEntry
      updateIdentifierEntry(id: ID!, input: UpdateIdentifierEntryInput!): IdentifierEntry
      deleteIdentifierEntry(id: ID!, userId: ID!): ID
    `,
  },
  controller: {
    mutations: {
      createIdentifierEntry: (_parent: any, args: any, context: any): Promise<IdentifierEntry | null> => {
        return createIdentifierEntryController(args.input, context);
      },
      updateIdentifierEntry: (_parent: any, args: any, context: any): Promise<IdentifierEntry | null> => {
        return updateIdentifierEntryController({ id: args.id, ...args.input }, context);
      },
      deleteIdentifierEntry: (_parent: any, args: any, context: any): Promise<string> => {
        return deleteIdentifierEntryController(args, context);
      },
    },
  }
});
