import { register } from './../core/register';
import { SignIn, User } from './../type/user';
import {
  signIn as signInController,
  findUser as findUserController,
  findUsers as findUsersController,
  initiateUser as initiateUserController,
  createUser as createUserController,
  updateUser as updateUserController,
  deleteUser as deleteUserController,
} from './../controller/user';

register({
  schema: {
    types: `
      enum ROLES {
        ROLE_USER
        ROLE_ADMIN
      }
    
      input FindUserInput {
        id: ID!
      }
    
      input CreateUserInput {
        email: String!
        role: ROLES!
        password: String!
      }
    
      input UpdateUserInput {
        email: String
        role: ROLES
      }
    
      type SignIn {
        token: String
      }
    
      type User {
        id: ID
        email: String
        hashedPassword: String
        role: ROLES
      }
    `,
    queries: `
      signIn(email: String!, password: String!): SignIn
      findUser(input: FindUserInput!): User
      findUsers: [User]
    `,
    mutations: `
      initiateUser(input: CreateUserInput!): User
      createUser(input: CreateUserInput!): User
      updateUser(id: ID!, input: UpdateUserInput!): User
      deleteUser(id: ID!): ID
    `,
  },
  controller: {
    queries: {
      signIn: (_parent: any, args: any, context: any): Promise<SignIn> => {
        return signInController(args, context);
      },
      findUser: (_parent: any, args: any, context: any): Promise<User | null> => {
        return findUserController(args.input, context);
      },
      findUsers: (_parent: any, args: any, context: any): Promise<User[] | null> => {
        return findUsersController({}, context);
      },
    },
    mutations: {
      initiateUser: async (_parent: any, args: any, context: any): Promise<User | null> => {
        return initiateUserController(args.input, context);
      },
      createUser: (_parent: any, args: any, context: any): Promise<User | null> => {
        return createUserController(args.input, context);
      },
      updateUser: (_parent: any, args: any, context: any): Promise<User | null> => {
        return updateUserController({ id: args.id, ...args.input }, context);
      },
      deleteUser: (_parent: any, args: any, context: any): Promise<string> => {
        return deleteUserController(args, context);
      },
    },
  }
});
