import './../domain/index';
import './migration';
import { ApolloServer } from 'apollo-server';
import { verifyJwt } from './security';
import { getResolvers, getTypeDefs } from './register';

const typeDefs = getTypeDefs();
const resolvers = getResolvers();
const context = ({ req }) => {
  const payload = verifyJwt(req.headers);
  const anonymousPayload = { id: null, role: 'ROLE_ANONYMOUSLY' };

  return payload ? { req, ...{ user: payload }} : { req, ...{ user: anonymousPayload }};
};

const server = new ApolloServer({ typeDefs, resolvers, context });

server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
