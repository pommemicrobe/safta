import './migration';
import { Database } from 'sqlite3';

const db: any = new Database(`${__dirname}/../safta.db`);

type Payload = { [key: string]: (string | number | boolean | number) };
type PrepareQuery = { body: string, parameters: (string | number | boolean)[] };
type QueryParams = (string | number | boolean)[];

export function query(query: string, params?: QueryParams): Promise<any> {
  const results: any = [];

  return new Promise((resolve, reject) => {
    db.all(query, params, (err: any, rows: any) => {
      if (err) return reject(err);

      rows.forEach((row: any) => {
        results.push(row);
      });

      resolve(results);
    });
  });
}

export function prepareInsert(payload: Payload): PrepareQuery {
  const fields: string[] = [];
  const values: string[] = [];
  const parameters: QueryParams = [];

  for (const [key, value] of Object.entries(payload)) {
    if (!value) continue;
 
    fields.push(key);
    values.push('?');
    parameters.push(value);
  }

  return {
    body: `(${fields.join(',')}) VALUES (${values.join(',')})`,
    parameters,
  }
}

export function prepareUpdate(payload: Payload): PrepareQuery {
  const body: string[] = [];
  const parameters: QueryParams = [];

  for (const [key, value] of Object.entries(payload)) {
    if (!value) continue;
 
    body.push(`${key}=?`);
    parameters.push(value);
  }

  return {
    body: body.join(','),
    parameters,
  }
}
