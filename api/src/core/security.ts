import * as crypto from 'crypto';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';

export type Roles = 'ROLE_ANONYMOUSLY' | 'ROLE_USER' | 'ROLE_ADMIN';

const jwtKeysDirectory = `${__dirname}/../documents/jwt-keys/`;

const roleHierarchies: { [key: string]: Roles[] } = {
  ROLE_ANONYMOUSLY: [],
  ROLE_USER: [],
  ROLE_ADMIN: ['ROLE_USER'],
};

function secretJwt(): string {
  const date = new Date();
  const currentYear = date.getFullYear();
  const currentMonth = date.getMonth() + 1 < 10 ? `0${ date.getMonth() + 1 }` : date.getMonth() + 1;
  const formattedDate = `${currentYear}-${currentMonth}`;
  const jwtKeysFilePath = `${jwtKeysDirectory}/${formattedDate}`;

  if (fs.existsSync(jwtKeysFilePath)) {
    return fs.readFileSync(jwtKeysFilePath).toString();
  }

  const privateKey = generateRandomKey(128);

  fs.writeFileSync(jwtKeysFilePath, privateKey);

  return privateKey;
}

export function generateRandomKey(length: number): string {
  return crypto.randomBytes(length).toString('hex').substr(0, length);
}

// export function encrypt(password: string, decrypted: string): string {
//   const key = crypto.scryptSync(password, 'salt', 32);
//   const cipher = crypto.createCipheriv('aes-256-cbc', key, Buffer.alloc(16, 0));

//   let encrypted = cipher.update(decrypted, 'utf8', 'hex');
//   encrypted += cipher.final('hex');

//   return encrypted;
// }

// export function decrypt(password: string, encrypted: string): string {
//   const key = crypto.scryptSync(password, 'salt', 32);
//   const decipher = crypto.createDecipheriv('aes-256-cbc', key, Buffer.alloc(16, 0));

//   let decrypted = decipher.update(encrypted, 'hex', 'utf8');
//   decrypted += decipher.final('utf8');

//   return decrypted;
// }

export function hash(text: string): string {
  const hmac = crypto.createHmac('sha512', text);
  const hashedText = hmac.digest('hex');
  hmac.end();
  return hashedText;
}

export function isLogged(context: any): boolean {
  return context.user && context.user.id;
}

export function isGranted(role: Roles, context: any): boolean {
  const userRole = context.user.role;

  if (userRole === role) return true;

  const userSubRoles = roleHierarchies[userRole];

  if (!userSubRoles.length) return false;

  for (const userSubRole of userSubRoles) {
    if (userSubRole === role) return true;
  }

  return false;
}

export function isOwner(userId: number, context: any): boolean {
  return context.user.id === userId;
}

export function verifyJwt(header: any): any {
  let token = header.authorization || '';

  if (!token) return null;

  token = token.substring(7);

  try {
    return jwt.verify(token, secretJwt());
  } catch {
    throw new Error('BAD_TOKEN');
  }
}

export function signJwt({ id, role }): any {
  const expiresIn = 3600; // TODO: Put in config file

  return jwt.sign({ id, role }, secretJwt(), { algorithm: 'HS512', expiresIn });
}
