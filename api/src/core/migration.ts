import * as fs from 'fs';
import { query } from './sqlite';

const configFile = `${__dirname}/../config/migration.json`;
const documentsDir = `${__dirname}/../migrations`;

function verify(): string[] {
  const lastMigration = getLastMigration();
  const files = fs.readdirSync(documentsDir);

  if (!lastMigration) return files;

  return files.filter((fileName: string) => {
    return parseInt(fileName) > lastMigration;
  });
}

function migrate(fileName: string): any {
  const queries = fs.readFileSync(`${documentsDir}/${fileName}`).toString().split('----------');
  
  for (const item of queries) {
    query(item);
  }
}

async function migrates(fileNames: string[]): Promise<void> {
  for (const fileName of fileNames) {
    await migrate(fileName);
  }
}

function getConfig(): any {
  if (!fs.existsSync(configFile)) {
    const data = { last_migration: null };
    fs.writeFileSync(configFile, JSON.stringify(data));
    return data;
  }
  return JSON.parse(fs.readFileSync(configFile).toString());
}

function getLastMigration(): number {
  const config = getConfig();
  return config.last_migration;
}

function updateLastMigration(version: number): void {
  const config = getConfig();
  config.last_migration = version;
  fs.writeFileSync(configFile, JSON.stringify(config));
}

async function start(): Promise<void> {
  const fileNames = verify();
  if (fileNames.length) {
    await migrates(fileNames);
    updateLastMigration(parseInt(fileNames[fileNames.length - 1]));
  }
}

start();
