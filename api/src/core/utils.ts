export function discard(data: any, paths: [string]): any | void {
  if (!Array.isArray(paths)) return data;

  for (const path of paths) {
    const fields = path.split('.');
  	const field = fields[0];
    
    if (fields.length === 1) delete data[field];
    else discard(data[field], [fields.splice(1).join('.')]);
  }
}
