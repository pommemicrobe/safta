import { gql } from 'apollo-server';

type resolvers = {
  Query: {
    [key: string]: (_parent: any, args: any, context: any) => Promise<any>,
  },
  Mutation: {
    [key: string]: (_parent: any, args: any, context: any) => Promise<any>,
  },
}

type schema = {
  queries?: string,
  mutations?: string,
  types?: string,
};

type controller = {
  queries?: {
    [key: string]: (_parent: any, args: any, context: any) => Promise<any>,
  },
  mutations?: {
    [key: string]: (_parent: any, args: any, context: any) => Promise<any>,
  },
};

type Config = {
  schema: schema,
  controller: controller,
};

let typeDefs: string = ``;
let resolvers: resolvers = {
  Query: {},
  Mutation: {},
};
let typeDefsQuery: string = ``;
let typeDefsMutation: string = ``;

export function getTypeDefs(): any {
  if (typeDefsQuery.length) typeDefs += `\ntype Query {\n${typeDefsQuery}\n}`;
  if (typeDefsMutation.length) typeDefs += `\ntype Mutation {\n${typeDefsMutation}\n}`;

  return gql`${typeDefs}`;
}

export function getResolvers(): resolvers {
  return resolvers;
}

export function register(config: Config): void {
  if (config.schema.queries) typeDefsQuery += `\n${config.schema.queries}`;
  if (config.schema.mutations) typeDefsMutation += `\n${config.schema.mutations}`;
  if (config.schema.types) typeDefs += `\n${config.schema.types}`;

  if (config.controller.queries) resolvers.Query = { ...resolvers.Query, ...config.controller.queries };
  if (config.controller.mutations) resolvers.Mutation = { ...resolvers.Mutation, ...config.controller.mutations };
}
