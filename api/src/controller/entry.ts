import { Entries } from './../type/entry';
import { findByUserId } from '../storage/identifier-entry';
import { isGranted, isOwner } from './../core/security';

export async function findEntries(args: any, context: any): Promise<Entries> {
  const { userId } = args;
  if (!isOwner(userId, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return {
    identifierEntries: await findByUserId(userId, context),
  };
}
