import { discard } from './../core/utils';
import { removeByUserId as removeIdentifierEntriesByUserId } from './../storage/identifier-entry';
import { create, find, findByEmail, findById, remove, update } from './../storage/user';
import { hash, isGranted, isLogged, isOwner, signJwt } from './../core/security';
import { SignIn, User } from './../type/user';

export async function signIn(args: any, context: any): Promise<SignIn> {
  const { email, password } = args;
  if (isLogged(context)) throw new Error('ALREADY_AUTHENTICATED');
  const user = await findByEmail(email, context);
  if (!user || user.hashedPassword !== hash(user.salt + password)) throw new Error('BAD_CREDENTIALS');
  return { token: signJwt({ id: user.id, role: user.role }) };
}

export function findUser(args: any, context: any): Promise<User | null> {
  const { id } = args;
  if (!isOwner(id, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return findById(id, context);
}

export function findUsers(args: any, context: any): Promise<User[] | null> {
  if (!isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return find(context);
}

export async function initiateUser(args: any, context: any): Promise<User | null> {
  const { email, password, role } = args;
  const users = await find(context);
  if (users) throw new Error('ALREADY_USED');
  return create(email, password, role, context);
}

export function createUser(args: any, context: any): Promise<User | null> {
  const { email, password, role } = args;
  if (!isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return create(email, password, role, context);
}

export function updateUser(args: any, context: any): Promise<User | null> {
  const { id, email, role } = args;
  if (!isOwner(id, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  if (!isGranted('ROLE_ADMIN', context)) args = discard(args, ['role']);
  return update(id, email, role, context);
}

export function deleteUser(args: any, context: any): Promise<string> {
  const { id } = args;
  if (!isOwner(id, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return Promise.all([
    remove(id, context),
    removeIdentifierEntriesByUserId(id, context),
  ]).then(() => id);
}
