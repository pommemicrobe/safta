import { IdentifierEntry } from './../type/identifier-entry';
import { isGranted, isOwner } from './../core/security';
import { create, remove, update } from './../storage/identifier-entry';

export function createIdentifierEntry(args: any, context: any): Promise<IdentifierEntry | null> {
  const { userId, name, url, encryptedUsername, encryptedPassword } = args;
  if (!isOwner(userId, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return create(userId, name, url, encryptedUsername, encryptedPassword, context);
}

export function updateIdentifierEntry(args: any, context: any): Promise<IdentifierEntry | null> {
  const { id, userId, name, url, encryptedUsername, encryptedPassword } = args;
  if (!isOwner(userId, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return update(id, userId, name, url, encryptedUsername, encryptedPassword, context);
}

export function deleteIdentifierEntry(args: any, context: any): Promise<string> {
  const { id, userId } = args;
  if (!isOwner(userId, context) && !isGranted('ROLE_ADMIN', context)) throw new Error('UNAUTHORIZED');
  return remove(id, userId, context);
}
