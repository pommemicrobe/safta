import { User, UserStorage } from './../type/user';

export function asUser(user: UserStorage): User {
  return {
    id: user.id,
    email: user.email,
    hashedPassword: user.hashed_password,
    role: user.role,
    salt: user.salt,
  }
}

export function asUsers(users: UserStorage[]): User[] {
  const usersFormatted: User[] = [];

  for (const user of users) {
    usersFormatted.push(asUser(user));
  }

  return usersFormatted;
}
