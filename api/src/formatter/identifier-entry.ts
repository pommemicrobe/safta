import {
  IdentifierEntry,
  IdentifierEntryStorage,
} from './../type/identifier-entry';

export function asIdentifierEntry(identifierEntry: IdentifierEntryStorage): IdentifierEntry {
  return {
    id: identifierEntry.id,
    name: identifierEntry.name,
    url: identifierEntry.url,
    encryptedUsername: identifierEntry.encrypted_username,
    encryptedPassword: identifierEntry.encrypted_password,
    userId: identifierEntry.user_id,
  };
}

export function asIdentifierEntries(identifierEntries: IdentifierEntryStorage[]): IdentifierEntry[] {
  return identifierEntries.map((identifierEntry: IdentifierEntryStorage) => asIdentifierEntry(identifierEntry));
}