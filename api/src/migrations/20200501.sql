CREATE TABLE IF NOT EXISTS users (
  id TEXT PRIMARY KEY,
  email TEXT CHECK(LENGTH(email) <= 100) NOT NULL UNIQUE,
  hashed_password TEXT CHECK(LENGTH(hashed_password) <= 255) NOT NULL,
  salt TEXT CHECK(LENGTH(salt) = 20) NOT NULL,
  role TEXT CHECK(role IN ('ROLE_USER', 'ROLE_ADMIN')) NOT NULL
);
----------
CREATE TABLE IF NOT EXISTS identifier_entries (
  id TEXT PRIMARY KEY,
  name TEXT CHECK(LENGTH(name) <= 100) NOT NULL,
  url TEXT CHECK(LENGTH(url) <= 255) DEFAULT NULL,
  encrypted_username TEXT CHECK(LENGTH(encrypted_username) <= 255) NOT NULL,
  encrypted_password TEXT CHECK(LENGTH(encrypted_password) <= 255) NOT NULL,
  user_id TEXT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id)
);