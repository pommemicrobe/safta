import { IdentifierEntry } from './../type/identifier-entry';
import { v4 as uuid } from 'uuid';
import { prepareInsert, prepareUpdate, query } from './../core/sqlite';
import { asIdentifierEntries, asIdentifierEntry } from './../formatter/identifier-entry';

function findById(id: string, context: any): Promise<IdentifierEntry | null> {
  return query('SELECT * FROM identifier_entries WHERE id = ?', [id])
    .then((identifierEntries) => identifierEntries[0] ? asIdentifierEntry(identifierEntries[0]) : null);
}

export function findByUserId(userId: string, context: any): Promise<IdentifierEntry[] | null> {
  return query('SELECT * FROM identifier_entries WHERE user_id = ?', [userId])
    .then((identifierEntries) => identifierEntries.length ? asIdentifierEntries(identifierEntries) : null);
}

export function create(
  userId: string,
  name: string,
  url: string,
  encryptedUsername: string,
  encryptedPassword: string,
  context: any
): Promise<IdentifierEntry | null> {
  const payload = {
    id: uuid(),
    name,
    url,
    encrypted_username: encryptedUsername,
    encrypted_password: encryptedPassword,
    user_id: userId,
  };
  const { body, parameters } = prepareInsert(payload);

  return query(`INSERT INTO identifier_entries ${body}`, parameters)
    .then(() => findById(payload.id, context));
}

export function update(
  id: string,
  userId: string,
  name: string,
  url: string,
  encryptedUsername: string,
  encryptedPassword: string,
  context: any
): Promise<IdentifierEntry | null> {
  const payload = {
    name,
    url,
    encrypted_username: encryptedUsername,
    encrypted_password: encryptedPassword,
  };
  const { body, parameters } = prepareUpdate(payload);
  return query(`UPDATE identifier_entries SET ${body} WHERE id = ? AND user_id = ?`, [...parameters, ...[id, userId]])
    .then(() => findById(id, context));
}

export function remove(id: string, userId: string, context: any): Promise<string> {
  return query('DELETE FROM identifier_entries WHERE id = ? AND user_id = ?', [id, userId])
    .then(() => id);
}

export function removeByUserId(userId: string, context: any): Promise<boolean> {
  return Promise.all([
    query('DELETE FROM identifier_entries WHERE user_id = ?', [userId]),
  ]).then(() => true);
}
