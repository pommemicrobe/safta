import { v4 as uuid } from 'uuid';
import { asUser, asUsers } from './../formatter/user';
import { generateRandomKey, hash, Roles } from './../core/security';
import { prepareInsert, prepareUpdate, query } from './../core/sqlite';
import { User, UserStorage } from './../type/user';

export function findByEmail(email: string, context: any): Promise<User | null> {
  return query('SELECT * FROM users WHERE email = ?', [email])
    .then((users: UserStorage[]) => users[0] ? asUser(users[0]) : null);
}

export function findById(id: string, context: any): Promise<User | null> {
  return query('SELECT * FROM users WHERE id = ?', [id])
    .then((users: UserStorage[]) => users[0] ? asUser(users[0]) : null);
}

export function find(context: any): Promise<User[] | null> {
  return query('SELECT * FROM users', [])
    .then((users: UserStorage[]) => users.length ? asUsers(users) : null);
}

export function create(email: string, password: string, role: Roles, context: any): Promise<User | null> {
  const salt = generateRandomKey(20);
  const payload = {
    id: uuid(),
    email,
    salt,
    hashed_password: hash(salt + password),
    role: role || 'ROLE_USER',
  };
  const { body, parameters } = prepareInsert(payload);

  return query(`INSERT INTO users ${body}`, parameters)
    .then(() => findById(payload.id, context));
}

export function update(id: string, email: string, role: Roles, context: any): Promise<User | null> {
  const payload = { email, role };
  const { body, parameters } = prepareUpdate(payload);

  return query(`UPDATE users SET ${body} WHERE id = ?`, [...parameters, id])
    .then(() => findById(id, context));
}

export async function remove(id: string, context: any): Promise<string> {
  return query('DELETE FROM users WHERE id = ?', [id])
    .then(() => id);
}
