import gql from 'graphql-tag';

export const FIND_ENTRIES = gql`
  query FindEntries($userId: ID!) {
    findEntries(userId: $userId) {
      identifierEntries {
        id
        name
        url
        encryptedUsername
        encryptedPassword
        userId
      }
    }
  }
`;

export const CREATE_IDENTIFIER_ENTRY = gql`
  mutation CreateIdentifierEntry($input: CreateIdentifierEntryInput!) {
    createIdentifierEntry(input: $input) {
      id
      name
      url
      encryptedUsername
      encryptedPassword
      userId
    }
  }
`;

export const UPDATE_IDENTIFIER_ENTRY = gql`
  mutation UpdateIdentifierEntry($id: ID!, $input: UpdateIdentifierEntryInput!) {
    updateIdentifierEntry(id: $id, input: $input) {
      id
      name
      url
      encryptedUsername
      encryptedPassword
      userId
    }
  }
`;

export const DELETE_IDENTIFIER_ENTRY = gql`
  mutation DeleteIdentifierEntry($id: ID!, $userId: ID!) {
    deleteIdentifierEntry(id: $id, userId: $userId)
  }
`;
