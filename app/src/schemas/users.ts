import gql from 'graphql-tag';

export const SIGN_IN = gql`
  query SignIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      token
    }
  }
`;

export const FIND_USER = gql`
  query FindUser($input: FindUserInput!) {
    findUser(input: $input) {
      id
      email
      hashedPassword
      role
    }
  }
`;

export const FIND_USERS = gql`
  query FindUsers {
    findUsers {
      id
      email
      hashedPassword
      role
    }
  }
`;

export const INITIATE_USER = gql`
  mutation InitiateUser($input: CreateUserInput!) {
    initiateUser(input: $input) {
      id
      email
      hashedPassword
      role
    }
  }
`;

export const CREATE_USER = gql`
  mutation CreateUser($input: CreateUserInput!) {
    createUser(input: $input) {
      id
      email
      hashedPassword
      role
    }
  }
`;

export const UPDATE_USER = gql`
  mutation UpdateUser($id: ID!, $input: UpdateUserInput!) {
    updateUser(id: $id, input: $input) { 
      id
      email
      hashedPassword
      role
    }
  }
`;

export const DELETE_USER = gql`
  mutation DeleteUser($id: ID!) {
    deleteUser(id: $id)
  }
`;
