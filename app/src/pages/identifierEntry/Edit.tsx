import { client } from '../../index';
import { getSessionItem } from '../../core/session';
import React from 'react';
import Spacer from '../../components/spacer/Spacer';
import { translate } from '../../core/translation';
import { useMutation } from '@apollo/react-hooks';
import { Alert, Button, Empty, Form, Input, Space } from 'antd';
import { CREATE_IDENTIFIER_ENTRY, FIND_ENTRIES, UPDATE_IDENTIFIER_ENTRY} from '../../schemas/entries';
import { decryptString, encryptString, generateRandomPassword } from '../../core/security';
import { Link, Redirect } from 'react-router-dom';

function Edit({ match }) {
  const { id } = match.params;
  const { findEntries }: any = client.readQuery({ query: FIND_ENTRIES, variables: { userId: getSessionItem('id') } });
  const [editIdentifierEntry, { loading, error, data }] = useMutation(
    id ? UPDATE_IDENTIFIER_ENTRY : CREATE_IDENTIFIER_ENTRY,
    {
      update: (cache, { data }) => {
        if (id) {
          const index = findEntries.identifierEntries.findIndex((item: any) => item.id === data.updateIdentifierEntry.id);
          if (index !== undefined) findEntries.identifierEntries[index] = data.updateIdentifierEntry;
        } else {
          if (!findEntries.identifierEntries) findEntries.identifierEntries = [data.createIdentifierEntry];
          else findEntries.identifierEntries.push(data.createIdentifierEntry);
        }
        cache.writeQuery({ query: FIND_ENTRIES, data: findEntries });
      },
    },
  );

  let identifierEntry;

  if (id) {
    identifierEntry = JSON.parse(JSON.stringify(findEntries.identifierEntries.find((item: any) => item.id === id)));

    if (!identifierEntry) return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;

    identifierEntry.encryptedUsername = decryptString(identifierEntry.encryptedUsername);
    identifierEntry.encryptedPassword = decryptString(identifierEntry.encryptedPassword);
  }

  const onFinish = (values: any) => {
    values.userId = getSessionItem('id');

    if (values.encryptedUsername) values.encryptedUsername = encryptString(values.encryptedUsername);
    if (values.encryptedPassword) values.encryptedPassword = encryptString(values.encryptedPassword);

    if (id) editIdentifierEntry({ variables: { id, input: values } });
    else editIdentifierEntry({ variables: { input: values } });
  }

  if (!loading && data) {
    return <Redirect to="/vault" />
  }

  return (
    <div id="identifier-entry" className="main">
      <h1>{ id ? translate('Update my identifier') : translate('Create an identifier') }</h1>

      { !loading && error && <><Alert message={error} type="error" showIcon /><Spacer size="large"></Spacer></> }

      <Form
        name="edit-identifier-form"
        onFinish={onFinish}
        initialValues={id ? identifierEntry : { encryptedPassword: generateRandomPassword() }}
        autoComplete="off"
      >  
        <Form.Item
          name="name"
          rules={[{ required: true, message: translate('Name required!') }]}
        >
          <Input placeholder={translate('Name')} />
        </Form.Item>

        <Form.Item
          name="url"
          rules={[{ type: 'url', message: translate('URL badly formatted (cf: https://domain.name)!') }]}
        >
          <Input placeholder={translate('Url')} />
        </Form.Item>

        <Form.Item
          name="encryptedUsername"
          rules={[{ required: true, message: translate('Username required!') }]}
        >
          <Input placeholder={translate('Username')} />
        </Form.Item> 

        <Form.Item
          name="encryptedPassword"
          rules={[{ required: true, message: translate('Password required!') }]}
        >
          <Input.Password placeholder={translate('Password')} autoComplete="off" />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit" loading={loading}>
              { id ? translate('Update') : translate('Create') }
            </Button>

            <Link to="/vault"><Button loading={loading}>{translate('Cancel')}</Button></Link>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Edit;
