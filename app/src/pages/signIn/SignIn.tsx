import  './SignIn.css';
import { decodeJwtToken } from '../../core/security';
import { Redirect } from 'react-router-dom';
import { SIGN_IN } from '../../schemas/users';
import Spacer from '../../components/spacer/Spacer';
import { translate } from '../../core/translation';
import { useLazyQuery } from '@apollo/react-hooks';
import { Alert, Button, Divider, Form, Input } from 'antd';
import { hasSession, populateSession } from '../../core/session';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import React, { useEffect } from 'react';

function SignIn({ setDisplayMenu }: any) {
  const [signIn, { error, loading, data }] = useLazyQuery(SIGN_IN);

  const onFinish = (values: any) => {
    signIn({ variables: values });
    populateSession(values);
  };

  if (!loading && data && data.signIn) {
    const token = data.signIn.token;
    const decodedToken = decodeJwtToken(token);
    const { id, role, exp } = decodedToken;

    populateSession({ id, role, token, exp });
  }

  useEffect(() => {
    if (hasSession()) setDisplayMenu(true);
  });

  if (hasSession()) return <Redirect to='/vault' />;

  return (
    <div id="sign-up-page">
      <h1>Safta</h1>

      <Divider />

      { !loading && error && <><Alert message={translate('Bad credentials')} type="error" showIcon /><Spacer size="large"></Spacer></> }

      <Form name="sign-up-form" onFinish={onFinish} autoComplete="off">
        <Form.Item
          name="email"
          rules={[{ required: true, message: translate('Email required!'), type: 'email' }]}
        >
          <Input
            prefix={<UserOutlined type="email" />}
            placeholder={translate('Email')}
            autoComplete="off"
          />
        </Form.Item>
        
        <Form.Item
          name="password"
          rules={[{ required: true, message: translate('Password required!') }]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder={translate('Password')}
            autoComplete="off"
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" loading={loading}>
            {translate('Sign up')}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default SignIn;
