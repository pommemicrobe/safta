import { FIND_ENTRIES } from '../../schemas/entries';
import { getSessionItem } from '../../core/session';
import IdentifierEntries from '../../components/identifierEntries/IdentifierEntries';
import React from 'react';
import { Tabs } from 'antd';
import { translate } from '../../core/translation';
import { useQuery } from '@apollo/react-hooks';

function Vault() {
  const { TabPane } = Tabs;
  const { loading, data } = useQuery(FIND_ENTRIES, { variables: { userId: getSessionItem('id') } });

  if (loading) return <div>{translate('Loading')}</div>;

  return (
    <Tabs defaultActiveKey="1" className="main">
      <TabPane tab={translate('Identifiers')} key="1">
        <IdentifierEntries identifierEntries={data.findEntries.identifierEntries} />
      </TabPane>

      {/* <TabPane tab="Files" key="2">
        <FileEntries />
      </TabPane> */}

      {/* <TabPane tab="Notes" key="3">
        <NoteEntries />
      </TabPane> */}
    </Tabs>
  );
}

export default Vault;
