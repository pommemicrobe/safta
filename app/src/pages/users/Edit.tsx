import React from 'react';
import { Button, Form, Input, Empty, Space, Alert, Select } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { Link, Redirect } from 'react-router-dom';
import Spacer from '../../components/spacer/Spacer';
import { translate } from '../../core/translation';
import { FIND_USERS, UPDATE_USER, CREATE_USER, FIND_USER } from '../../schemas/users';
import { client } from '../../index';

const { Option } = Select;

function Edit({ match }) {
  const { id } = match.params;
  const { findUsers }: any = client.readQuery({ query: FIND_USERS });
  const [editUser, { loading, error, data }] = useMutation(
    id ? UPDATE_USER : CREATE_USER,
    {
      update: (cache, { data }) => {
        if (id) {
          const index = findUsers.findIndex((item: any) => item.id === data.updateUser.id);
          if (index !== undefined) findUsers[index] = data.updateUser;
        } else {
          findUsers.push(data.createUser);
        }
        cache.writeQuery({ query: FIND_USERS, data: findUsers });
      },
    },
  );

  let user;

  if (id) {
    user = JSON.parse(JSON.stringify(findUsers.find((item: any) => item.id === id)));
    if (!user) return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
  }

  const onFinish = (values: any) => {
    if (id) editUser({ variables: { id, input: values } });
    else editUser({ variables: { input: values } });
  }

  if (!loading && data) {
    return <Redirect to="/users" />
  }

  return (
    <div id="user" className="main">
      <h1>{ id ? translate('Update this user') : translate('Create an user') }</h1>

      { !loading && error && <><Alert message={error} type="error" showIcon /><Spacer size="large"></Spacer></> }

      <Form
        name="edit-user-form"
        onFinish={onFinish}
        initialValues={id ? user : {}}
        autoComplete="off"
      >  
        <Form.Item
          name="email"
          rules={[{ required: true, message: translate('Email required!') }]}
        >
          <Input placeholder={translate('Email')} />
        </Form.Item>

        {
          !id &&
          <Form.Item
            name="password"
            rules={[{ required: true, message: translate('Password required!') }]}
          >
            <Input placeholder={translate('Password')} />
          </Form.Item>
        }

        <Form.Item
          name="role"
          rules={[
            { required: true, message: translate('Role required!') },
            { type: 'enum', enum: ['ROLE_USER', 'ROLE_ADMIN'] }
          ]}
        >
          <Select placeholder={translate('Role')}>
            <Option value="ROLE_USER">{translate('User')}</Option>
            <Option value="ROLE_ADMIN">{translate('Admin')}</Option>
          </Select>
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit" loading={loading}>
              { id ? translate('Update') : translate('Create') }
            </Button>

            <Link to="/users"><Button loading={loading}>{translate('Cancel')}</Button></Link>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Edit;
