import { Link } from 'react-router-dom';
import React from 'react';
import Spacer from '../../components/spacer/Spacer';
import { translate } from '../../core/translation';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { Button, Space, Table } from 'antd';
import { DELETE_USER, FIND_USERS } from '../../schemas/users';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';

function Users() {
  const findUsersResult = useQuery(FIND_USERS);
  const [deleteUser, deleteUserResult] = useMutation(
    DELETE_USER,
    {
      update: (cache, { data }) => {
        const findUsers: any = cache.readQuery({ query: FIND_USERS});
        const index = findUsers.findUsers.findIndex((item: any) => item.id === data.deleteUser);
        findUsers.findUsers.splice(index, 1);
        cache.writeQuery({ query: FIND_USERS, data: findUsers });
      },
    },
  );

  let usersData = [];

  if (!findUsersResult.loading && findUsersResult.data) {
    usersData = findUsersResult.data.findUsers.map((item: any) => {
      return {
        key: item.id,
        email: item.email,
        role: item.role,
      }
    });
  }

  const columns = [
    {
      title: translate('Email'),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: translate('Role'),
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: translate('Actions'),
      key: 'action',
      render: (text, record) => (
        <Space>
          <Link to={`/user/${record.key}`}>
            <Button><EditOutlined /></Button>
          </Link>
          <Button
            danger
            onClick={() => deleteUser({ variables: { id: record.key }})}
            loading={deleteUserResult.loading}
          >
            <DeleteOutlined />
          </Button>
        </Space>
      ),
    }
  ];

  return (
    <div id="users-page" className="main">
      <Space>
        <Link to="/user"><Button><PlusOutlined /></Button></Link>
      </Space>

      <Spacer />
      
      <Table
        columns={columns}
        dataSource={usersData}
        loading={findUsersResult.loading}
        pagination={false}
      />
    </div>
  );
}

export default Users;
