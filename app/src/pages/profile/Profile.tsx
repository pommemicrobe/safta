import { getSession } from '../../core/session';
import React from 'react';
import { Redirect } from 'react-router-dom';
import { translate } from '../../core/translation';
import { useMutation } from '@apollo/react-hooks';
import { Button, Form, Input, Space } from 'antd';
import { DELETE_USER, UPDATE_USER } from '../../schemas/users';

function Profile() {
  const user = getSession();
  const [updateUser, updateUserResult] = useMutation(UPDATE_USER);
  const [deleteUser, deleteUserResult] = useMutation(DELETE_USER);

  const onFinish = (values: any) => {
    updateUser({ variables: { id: user.id, input: values }});
  }

  const onClickDelete = () => {
    deleteUser({ variables: { id: user.id }});
  }

  if (!deleteUserResult.loading && deleteUserResult.data) {
    return <Redirect to="/logout" />
  }

  return (
    <Form
      name="update-profile-form"
      onFinish={onFinish}
      initialValues={user}
      autoComplete="off"
      className="main"
    >  
      <Form.Item
        name="email"
        rules={[{ required: true, message: translate('Email required!') }]}
      >
        <Input placeholder={translate('Email')} />
      </Form.Item>

      <Form.Item>
        <Space>
          <Button
            type="primary"
            htmlType="submit"
            loading={updateUserResult.loading || deleteUserResult.loading}
          >
            {translate('Update')}
          </Button>

          <Button
            danger
            loading={updateUserResult.loading || deleteUserResult.loading}
            onClick={onClickDelete}
          >
            {translate('Delete')}
          </Button>
        </Space>
      </Form.Item>
    </Form>
  )
}

export default Profile;
