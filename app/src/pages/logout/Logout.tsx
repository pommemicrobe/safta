import { closeSession } from '../../core/session';
import { Redirect } from 'react-router-dom';
import React, { useEffect } from 'react';

function Logout({ setDisplayMenu }: any) {
  closeSession();

  useEffect(() => {
    setDisplayMenu(false);
  });

  return <Redirect to='/' />;
}

export default Logout;
