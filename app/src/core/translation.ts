import config from '../core/config';
import { translations } from '../translations/index';

export function translate(key: string, locale?: string) {
  let translation;

  if (!locale) locale = config.translation.default;
  if (locale) translation = translations[locale];
  if (!translation) return key;

  const trans = translation.find((item: any) => item?.key === key);

  return trans ? trans.value : key;
}
