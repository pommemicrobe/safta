import { Session } from './types';

let session: Session = {
  id: null,
  email: null,
  password: null,
  role: null,
  token: null,
  isActive: false,
  expiredAt: null,
};

export function populateSession({ id, email, password, role, token, exp}: any): void {
  session = {
    id: id ?? session.id,
    email: email ?? session.email,
    password: password ?? session.password,
    role: role ?? session.role,
    token: token ?? session.token,
    expiredAt: exp ?? session.expiredAt,
  };

  startSession();
}

export function startSession(): void {
  if (session.id
    && session.email
    && session.password
    && session.role
    && session.token
    && session.expiredAt) session.isActive = true;
}

export function closeSession(): void {
  session = {
    id: null,
    email: null,
    password: null,
    role: null,
    token: null,
    isActive: false,
    expiredAt: null,
  };
}

export function isExpired(): boolean {
  const currentTimestamp = (new Date()).getTime();
  return session.expiredAt ? currentTimestamp > session.expiredAt * 1000 : false;
}

export function hasSession(): boolean {
  return session?.isActive === true;
}

export function getSession(): Session {
  if (!hasSession()) throw Error('Session not found!');
  return session;
}

export function getSessionItem(key: string): string {
  if (!hasSession()) throw Error('Session not found!');
  if (!session[key]) throw Error('Session item not found!');
  return session[key];
}
