export type Session = {
  id: string | null
  email: string | null
  password: string | null
  token: string | null
  role: string | null
  isActive?: boolean
  expiredAt: number | null
}
