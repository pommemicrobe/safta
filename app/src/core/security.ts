import aesjs from 'aes-js';
import JwtDecode from 'jwt-decode';
import * as pkcs7 from 'pkcs7';
import { getSession, getSessionItem, hasSession } from './session';

const passwordCharacters = {
  upperChars: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'q', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
  lowerChars: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'Q', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
  numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
  specials: ['&', '#', '{', '(', '[', '^', '@', ')', ']', '°', '}', '$', '%', '!'],
};

const roleHierarchies: RoleHierarchies = {
  ROLE_ANONYMOUSLY: [],
  ROLE_USER: [],
  ROLE_ADMIN: ['ROLE_USER'],
};

export type Roles = 'ROLE_ANONYMOUSLY' | 'ROLE_USER' | 'ROLE_ADMIN';
export type RoleHierarchies = { [key: string]: Roles[] };

export function decodeJwtToken(token: string): any {
  return JwtDecode(token);
}

function shuffle(array: any[]): any[] {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * i);
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

  return array;
}

export function isGranted(role: Roles): boolean {
  if (hasSession()) return false;

  const session = getSession();
  const userRole = session.role;

  if (!userRole) return false;
  if (userRole === role) return true;

  const userSubRoles = roleHierarchies[userRole];

  if (!userSubRoles.length) return false;

  for (const userSubRole of userSubRoles) {
    if (role === userSubRole) return true;
  }

  return false;
}

export function encryptString(decrypted: string): string {
  const password = getSessionItem('password');
  return encrypt(password, decrypted);
}

export function decryptString(encrypted: string): string {
  const password = getSessionItem('password');
  return decrypt(password, encrypted);
}

export function encrypt(password: string, decrypted: string): string {
  const key = Buffer.alloc(32, password, 'utf8');
  const iv = Buffer.alloc(16, 0);
  const decryptedBytes = pkcs7.pad(aesjs.utils.utf8.toBytes(decrypted));

  const aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  const encryptedBytes = aesCbc.encrypt(decryptedBytes);

  return aesjs.utils.hex.fromBytes(encryptedBytes);
}

export function decrypt(password: string, encrypted: string): string {
  const key = Buffer.alloc(32, password, 'utf8');
  const iv = Buffer.alloc(16, 0);
  const encryptedBytes = aesjs.utils.hex.toBytes(encrypted);

  const aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  const decryptedBytes = aesCbc.decrypt(encryptedBytes);

  return aesjs.utils.utf8.fromBytes(pkcs7.unpad(decryptedBytes));
}

export function generateRandomPassword(
  upperChars = true,
  numbers = true,
  specials = true,
  length = 26
): string {
  let chars: any[] = passwordCharacters.lowerChars;
  let password = '';

  if (upperChars) chars = [...passwordCharacters.upperChars, ...chars];
  if (numbers) chars = [...passwordCharacters.numbers, ...chars];
  if (specials) chars = [...passwordCharacters.specials, ...chars];

  for (let i = 0; i < length; i++) {
    chars = shuffle(chars);
    password += chars[Math.floor(Math.random() * chars.length)];
  }

  return password;
}
