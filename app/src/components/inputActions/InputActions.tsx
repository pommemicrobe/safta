import './InputActions.css';
import React from 'react';
import { render } from 'react-dom';
import { CopyOutlined, EyeInvisibleOutlined, EyeOutlined, LinkOutlined } from '@ant-design/icons';
import { Input, Button } from 'antd';

function InputActions({ defaultValue, type, actions, placeholder, disabled }: any) {
  if (!actions) throw Error('Actions missing!');
  if (!defaultValue) defaultValue = '';
  if (!type) type = 'text';
  if (!placeholder) placeholder = '';
  if (!disabled) disabled = false;

  return <Input
    className="input-with-actions"
    disabled={disabled}
    type={type}
    placeholder={placeholder}
    defaultValue={defaultValue}
    addonAfter={createButtons(actions)}
  />;
}

function createButtons({ openLink, displayPassword, copy }: any) {
  return (
    <div>
      { openLink && <Button onClick={onClickOpenLink}><LinkOutlined /></Button> }
      { displayPassword && <Button onClick={onClickDisplayPassword}><EyeOutlined /></Button> }
      { copy && <Button onClick={onClickCopy}><CopyOutlined /></Button> }
    </div>
  );
}

function onClickOpenLink(ev: any) {
  const input = findInput(ev.currentTarget);
  window.open(input.value, '_blank');
}

function onClickDisplayPassword(ev: any) {
  const currentTarget = ev.currentTarget;
  const input = findInput(currentTarget);

  if (input.type === 'password') {
    input.type = 'text';
    render(<EyeInvisibleOutlined />, currentTarget);
  } else {
    input.type = 'password';
    render(<EyeOutlined />, currentTarget);
  }
}

function onClickCopy(ev: any) {
  const input = findInput(ev.currentTarget);
  navigator.clipboard.writeText(input.value);
}

function findInput(currentTarget: any) {
  const parent = currentTarget.closest('.input-with-actions');
  const input = parent.querySelector('input');

  return input;
}

export default InputActions;
