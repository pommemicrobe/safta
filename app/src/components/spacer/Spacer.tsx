import './Spacer.css';
import React from 'react';

function Spacer({ size }: any) {
  const sizes = ['small', 'normal', 'large'];

  if (!size && !sizes.includes(size)) size = 'spacer-normal';
  else size = `spacer-${size}`;

  return <div className={`spacer ${size}`}></div>;
}

export default Spacer;
