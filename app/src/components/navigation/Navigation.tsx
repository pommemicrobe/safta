import './Navigation.css';
import { Link } from 'react-router-dom';
import { LogoutOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import React from 'react';
import { translate } from '../../core/translation';

function Navigation() {
  return (
    <Menu mode="horizontal" defaultSelectedKeys={['vault']}>
      <Menu.Item key="vault">
        <Link to="/vault">{translate('Vault')}</Link>
      </Menu.Item>

      <Menu.Item key="users">
        <Link to="/users">{translate('Users')}</Link>
      </Menu.Item>

      <Menu.Item key="profile">
        <Link to="/profile">{translate('Profile')}</Link>
      </Menu.Item>

      <Menu.Item key="logout" id="navigation-logout">
        <Link to="/logout"><LogoutOutlined /></Link>
      </Menu.Item>
    </Menu>
  );
}

export default Navigation;
