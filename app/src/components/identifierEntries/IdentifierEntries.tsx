import './IdentifierEntries.css';
import { decryptString } from '../../core/security';
import { getSessionItem } from '../../core/session';
import InputActions from '../inputActions/InputActions';
import { Link } from 'react-router-dom';
import React from 'react';
import Spacer from '../spacer/Spacer';
import { translate } from '../../core/translation';
import { useMutation } from '@apollo/react-hooks';
import { Button, Card, Col, Input, Row, Space } from 'antd';
import { DELETE_IDENTIFIER_ENTRY, FIND_ENTRIES } from '../../schemas/entries';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';

const { Search } = Input;

function IdentifierEntry({ identifierEntries }: any) {
  const [deleteIdentifierEntry, { loading }] = useMutation(
    DELETE_IDENTIFIER_ENTRY,
    {
      update: (cache, { data }) => {
        const entries: any = cache.readQuery({ query: FIND_ENTRIES, variables: { userId: getSessionItem('id') } });
        const index = entries.findEntries.identifierEntries.findIndex((item: any) => item.id === data.deleteIdentifierEntry);
        entries.findEntries.identifierEntries.splice(index, 1);
        cache.writeQuery({ query: FIND_ENTRIES, data: entries });
      },
    },
  );

  if (!identifierEntries || !identifierEntries.length) return <Link to="/identifier"><Button>{translate('New identifier')}</Button></Link>;

  const onClickDelete = (ev) => {
    const id = ev.currentTarget.dataset['id'];
    const userId = getSessionItem('id');
    deleteIdentifierEntry({ variables: { id, userId } });
  };

  return (
    <div id="identifierEntries">
      <Space>
        <Search
          placeholder={translate('Search')}
          onChange={onChangeSearch}
        />

        <Link to="/identifier">
          <Button><PlusOutlined /></Button>
        </Link>
      </Space>

      <Spacer size="large" />

      <Row>
        {identifierEntries.map((identifierEntry) => {
          return <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6} key={identifierEntry.id}>
            <Card
              size="small"
              title={identifierEntry.name} data-search={identifierEntry.name}
              extra={<Space>
                <Link to={`/identifier/${identifierEntry.id}`}>
                  <Button><EditOutlined /></Button>
                </Link>

                <Button
                  danger
                  onClick={onClickDelete}
                  data-id={identifierEntry.id}
                  loading={loading}
                >
                  <DeleteOutlined />
                </Button>
              </Space>}
            >
              <InputActions
                defaultValue={identifierEntry.url}
                disabled={true}
                actions={{ openLink: true }}
              />

              <Spacer />

              <InputActions
                defaultValue={decryptString(identifierEntry.encryptedUsername)}
                disabled={true}
                actions={{ copy: true }}
              />

              <Spacer />

              <InputActions
                defaultValue={decryptString(identifierEntry.encryptedPassword)}
                disabled={true}
                type="password"
                actions={{ displayPassword: true, copy: true }}
              />
            </Card>
          </Col>
        })}
      </Row>
    </div>
  );
}

function onChangeSearch(e) {
  const value = e.currentTarget.value.toLowerCase();

  document.querySelectorAll('#identifierEntries .ant-card').forEach((item: any) => {
    if (item.dataset.search.toLowerCase().lastIndexOf(value) === -1) item.style.display = 'none';
    else item.style.display = 'block';
  });
}

export default IdentifierEntry;
