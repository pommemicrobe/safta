import './index.css';
import App from './App';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import config from './core/config';
import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { closeSession, getSessionItem, hasSession, isExpired } from './core/session';

export const client = new ApolloClient({
  uri: config.api.url,
  request: (operation) => {
    if (hasSession()) {
      if (isExpired()) {
        closeSession();
        window.location.href = '/';
        return;
      }

      const token = getSessionItem('token');

      operation.setContext({
        headers: {
          authorization: token ? `Bearer ${token}` : '',
        }
      }); 
    }
  },
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
