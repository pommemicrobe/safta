import './App.css';
import { hasSession } from './core/session';
import IdentifierEntryEdit from './pages/identifierEntry/Edit';
import Logout from './pages/logout/Logout';
import Navigation from './components/navigation/Navigation';
import Profile from './pages/profile/Profile';
import SignIn from './pages/signIn/SignIn';
import UserEdit from './pages/users/Edit';
import Users from './pages/users/Users';
import Vault from './pages/vault/Vault';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import React, { useState } from 'react';

function App() {
  const [displayMenu, setDisplayMenu] = useState(false);

  if (!hasSession() && window.location.pathname !== '/') {
    window.location.pathname = '/';
    return <div></div>;
  }

  return (
    <Router>
      { displayMenu && <Navigation /> }

      <Switch>
        <Route path="/vault" component={Vault}></Route>
        <Route path="/users" component={Users}></Route>
        <Route path="/user/:id?" component={UserEdit}></Route>
        <Route path="/profile" component={Profile}></Route>
        <Route path="/identifier/:id?" component={IdentifierEntryEdit}></Route>
        <Route path="/logout">
          <Logout setDisplayMenu={setDisplayMenu} />
        </Route>
        <Route path="/">
          <SignIn setDisplayMenu={setDisplayMenu} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
